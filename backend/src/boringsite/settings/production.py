from .bade import *

# The server can be reached from localhost
ALLOWED_HOSTS = os.environ['DJANGO_ALLOWED_HOSTS'].split(',')

# SECURITY WARNING: Keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

DEBUG = False

STATIC_URL = "/static/"

# The location where the static assets of the collectstatic command will be stored.
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Additional locations to look for static content when running collectstatic.
# When in DEBUG mode these directories will also be searched for static content.
STATICFILES_DIRS = []

##
# Database Settings
##
# Dit moet later mysql worden.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DJANGO_DATABASE_NAME'],
        'USER': os.environ['DJANGO_DATABASE_USER'],
        'PASSWORD': os.environ['DJANGO_DATABASE_PASSWORD'],
        'HOST': 'postgres',
        'PORT': os.environ['DJANGO_DATABASE_PORT'],
    }
}
