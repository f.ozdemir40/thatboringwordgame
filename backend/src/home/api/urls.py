from django.urls import include, path
from rest_framework import routers

from .views import CategoryViewSet, QuestionViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'questions', QuestionViewSet)

urlpatterns = [
    path('', include(router.urls))
]