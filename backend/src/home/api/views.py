from rest_framework import viewsets, permissions

from .serializers import CategorySerializer, QuestionSerializer
from home.models import Category, Question


#Category Views
class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = (permissions.IsAdminUser,)

# Question Views
class QuestionViewSet(viewsets.ModelViewSet):
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()
    permission_classes = (permissions.IsAdminUser,)
