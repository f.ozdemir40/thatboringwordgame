from django.db import models

# Create your models here.


class Category(models.Model):
    category = models.CharField(max_length=70)

    class Meta:
        verbose_name = "category"
        verbose_name_plural = "categories"

    def __str__(self):
        return self.category


class Question(models.Model):
    question = models.CharField(max_length=250)
    answer = models.CharField(max_length=90)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "question"
        verbose_name_plural = "questions"

    def __str__(self):
        return self.question
