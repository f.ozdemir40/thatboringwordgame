# That boring word game.
Ideas came up when bored. Developed when bored. Man this game is really boring...

**Requirements**
>Python 3
>Pip & virtual environment

This art project is developed by Furkan Ozdemir


##### Setting up project 
Once you have activated your virtual environment for this project. Go with your terminal into the directory that has the requirements.txt file. Type in your terminal:
```
pip install -r requirements.txt
```

This will install the Django version that has been used for this project.


##### How to start the project
There are two files in the settings directory called *development.py* and *production.py*. The file you need is the development python file. Once you cloned this repository in your directory. Start up
