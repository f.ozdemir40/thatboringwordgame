let body = document.body;
let forwardLink = document.getElementById('forward-link');

forwardLink.onmouseover = function () {
    body.className = "change-body";
}

forwardLink.onmouseout = function () {
    body.className = "";
}

// //Backward link configuration
// let backwardLink = document.getElementById('message-1')

// $(backwardLink).click(function(e) {
//     e.preventDefault();
//     setTimeout(function () {
//         window.close();
//     }, 2000);
// });

//Forward link configuration
$("a.forward-link[href]").click(function (e) {
    e.preventDefault();
    if (this.href) {
        let target = this.href;
        setTimeout(function () {
            window.location = target;
        }, 1000);
    }
});


